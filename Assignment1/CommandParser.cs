﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections;

namespace Assignment1
{
    class CommandParser
    {
        public bool isrunning = false;
        public bool checksyntax = false;
        public bool terminate = false;

        int lineoffset = 0;

        ArrayList storeShapes = new ArrayList();

        ShapeFactory factory;

        Shape shape;

        Canvas canvas;

        Color colour;
        int x, y;

        public CommandParser(Canvas canvas)
        {
            this.canvas = canvas;
            factory = new ShapeFactory();
        }
        public ArrayList validateCmd(string cmd,  int lineoffset)
        {
            Console.WriteLine("****Inside validateCmd*****");

            String[] splitLine = cmd.Split('\n');

            if(splitLine.Length>0)
            {
                for(int i=0; i<splitLine.Length; i++)
                {
                    String[] splitKey = splitLine[i].Split(' ');

                    shape = factory.getShape(splitKey[0]);

                    if (shape != null)
                    {
                        String[] splitValues = splitKey[1].Split(',');
                        try 
                        {
                            if (splitKey[0].ToLower().Trim().Equals("circle") && splitValues.Length == 1)
                            {
                                int radius = Int32.Parse(splitValues[0]);

                                shape.set(colour, x, y, radius);

                                storeShapes.Add(shape);

                            }
                            else if (splitValues.Length == 2)
                            {
                                int parameter1 = Int32.Parse(splitValues[0]);
                                int parameter2 = Int32.Parse(splitValues[1]);

                                if (splitKey[0].ToLower().Trim().Equals("moveto"))
                                {
                                    
                                }
                                else if (splitKey[0].ToLower().Trim().Equals("drawto"))
                                {

                                }
                                else
                                {
                                    shape.set(colour, x, y, parameter1, parameter2);
                                    storeShapes.Add(shape);
                                }
                            }
                        }
                        catch { }
                        }
                    

                }
            }
            return storeShapes;
        }
    }
}
