﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace Assignment1
{
    public partial class ShapeCreation : Form
    {
        Bitmap OutputBitmap = new Bitmap(1000, 1000);

        Bitmap temp_screen = new Bitmap(1000, 1000);

        CommandParser parser;

        Canvas canvas;
        
        public ShapeCreation()
        {
            InitializeComponent();
            canvas = new Canvas(Graphics.FromImage(OutputBitmap), display);
            parser = new CommandParser(canvas);//class for handling the drawing, pass the drawing surface to it
            canvas.DrawCursor(Graphics.FromImage(temp_screen));
            canvas.SetRunBtn(button_execute);
            canvas.SetTextBox(console);
        }
        private void ShapeCreation_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        //method for checking if enter is pressed in the single line command
        private void cmdLine_KeyDown(object sender, KeyEventArgs e)
        {
            //checking if the enter has been pressed and the text written inside is run or not
            if (e.KeyCode == Keys.Enter && !cmdLine.Text.ToLower().Equals(""))
            {
                //if the condition holds true then the button should be clicked to performe further actions 
                button_execute.PerformClick();
            }
            
        }

        //when the button is clicked this method is called
        private void button_execute_Click(object sender, EventArgs e)
        {
            // if checks weather of not the single line command text box has run or not
            if (cmdLine.Text.ToLower().Trim().Equals("run")) 
            {
                string cmd = cmdText.Text;

                storeShapes = parser.validateCmd(cmd);

                //storing the multi line command inside a string form the cmdText textbox
             }
            
        }

        private void display_Paint(object sender, PaintEventArgs e)
        {
            Console.WriteLine("inside ");

            Graphics g = e.Graphics;
            for (int i = 0; i < storeShapes.Count; i++)
            {
                Shape s;
                s = (Shape)storeShapes[i];
                if (s != null)
                {
                    s.draw(g, false);
                    
                }
                else
                    console.Text = "invalid shape in array"; //shouldn't happen as factory does not produce rubbish

            }
        }
    }
}
