﻿
namespace Assignment1
{
    partial class ShapeCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdText = new System.Windows.Forms.TextBox();
            this.cmdLine = new System.Windows.Forms.TextBox();
            this.button_execute = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.console = new System.Windows.Forms.TextBox();
            this.display = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.display)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdText
            // 
            this.cmdText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.cmdText.Location = new System.Drawing.Point(491, 40);
            this.cmdText.Multiline = true;
            this.cmdText.Name = "cmdText";
            this.cmdText.Size = new System.Drawing.Size(297, 259);
            this.cmdText.TabIndex = 0;
            this.cmdText.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // cmdLine
            // 
            this.cmdLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.cmdLine.Location = new System.Drawing.Point(573, 322);
            this.cmdLine.Multiline = true;
            this.cmdLine.Name = "cmdLine";
            this.cmdLine.Size = new System.Drawing.Size(216, 38);
            this.cmdLine.TabIndex = 1;
            this.cmdLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmdLine_KeyDown);
            // 
            // button_execute
            // 
            this.button_execute.Location = new System.Drawing.Point(491, 322);
            this.button_execute.Name = "button_execute";
            this.button_execute.Size = new System.Drawing.Size(75, 37);
            this.button_execute.TabIndex = 2;
            this.button_execute.Text = "Execute";
            this.button_execute.UseVisualStyleBackColor = true;
            this.button_execute.Click += new System.EventHandler(this.button_execute_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Honeydew;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(493, 370);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Console";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // console
            // 
            this.console.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.console.Location = new System.Drawing.Point(492, 401);
            this.console.Multiline = true;
            this.console.Name = "console";
            this.console.ReadOnly = true;
            this.console.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.console.Size = new System.Drawing.Size(297, 113);
            this.console.TabIndex = 4;
            this.console.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.Color.Honeydew;
            this.display.Location = new System.Drawing.Point(12, 40);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(467, 473);
            this.display.TabIndex = 5;
            this.display.TabStop = false;
            this.display.Paint += new System.Windows.Forms.PaintEventHandler(this.display_Paint);
            // 
            // ShapeCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(800, 525);
            this.Controls.Add(this.display);
            this.Controls.Add(this.console);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_execute);
            this.Controls.Add(this.cmdLine);
            this.Controls.Add(this.cmdText);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "ShapeCreation";
            this.Text = "Shape Creating Programing Language";
            this.Load += new System.EventHandler(this.ShapeCreation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.display)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox cmdText;
        private System.Windows.Forms.TextBox cmdLine;
        private System.Windows.Forms.Button button_execute;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox display;
        private System.Windows.Forms.TextBox console;
    }
}

