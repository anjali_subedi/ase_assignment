﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;

namespace Assignment1
{
    class Canvas
    {
        // Delegate for providing reference to the picture box.
        public delegate void RefereshDisplayDelegate();

        // Delegate for providing reference to the run button.
        public delegate void RunBtnDelegate(bool value);

        // Delegate for providing reference to the textarea
        public delegate void TextAreaDelegate(StringBuilder text);

        PictureBox display;
        Button runBtn;
        TextBox errorBox;

        public Graphics g;

        public SolidBrush Brush;

        public int xPos, yPos;

        public Color colour;

        public bool fill = false;

        public bool run = false;

        // Initializes all the instance variable
        public Canvas(Graphics g, PictureBox display)
        {

            this.g = g; //for main 
            xPos = yPos = 210;
            colour = Color.Black;
            Brush = new SolidBrush(colour);
            this.display = display;
        }

        public void RefereshDisplay()
        {
            display.Invalidate();
        }
        public void WriteErrors(StringBuilder errors)
        {
            this.errorBox.Text = errors.ToString();
        }
        public void BtnStatus(bool value)
        {
            runBtn.Enabled = value;
        }
        public void SetRunBtn(Button btn)
        {
            this.runBtn = btn;
        }
        public void SetTextBox(TextBox textbox)
        {
            this.errorBox = textbox;
        }

        public void InvokeBtnDelegate(bool value)
        {
            try
            {
                runBtn.Invoke(new RunBtnDelegate(BtnStatus), value);
            }
            catch (InvalidOperationException)
            {

            }

        }

        public void InvokeConsoleDelegate(StringBuilder text)
        {
            try
            {
                runBtn.Invoke(new TextAreaDelegate(WriteErrors), text);
            }
            catch (InvalidOperationException)
            {

            }

        }

        public void InvokeDelegate()
        {
            try
            {
                display.Invoke(new RefereshDisplayDelegate(RefereshDisplay));
            }
            catch (InvalidOperationException)
            {

            }

        }

        public void SetBrushAndPenColor(String colorName)
        {
            this.colour = Color.FromName(colorName);
            Brush.Color = colour;

        }

        public void DrawLine(int[] xy)
        {
            "Drawline Responded from the Canvas!" + xPos + " " + yPos + " " + xy[0] + " " + xy[1]);
            //draw a line from current (xPos, yPos) to provided arguments (toX, toY)
            g.DrawLine(new Pen(colour), xPos, yPos, xy[0], xy[1]);//draw line
            xPos = xy[0];
            yPos = xy[1];
        }

        public void ChangeCursorLocation(int[] xy)
        {
            xPos = xy[0];
            yPos = xy[1];
        }

        public void Clear()
        {
            g.Clear(Color.Transparent);

        }
        public void Reset()
        {
            xPos = yPos = 210;
            fill = false;
            colour = Color.Black;
            g.Clear(Color.Transparent);
        }
        public void DrawCursor(Graphics g)
        {
            SolidBrush brush = new SolidBrush(Color.Green);


            g.Clear(Color.Transparent);
            g.FillEllipse(brush, xPos - 2, yPos - 2, 2 * 2, 2 * 2);
        }
    }
}
